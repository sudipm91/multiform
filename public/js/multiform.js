

	$(document).ready(function(){
	    var maxField = 10; //Input fields increment limitation
	    var addButton = $('.add_button'); //Add button selector
	    var wrapper = $('.field_wrapper'); //Input field wrapper
	    var fieldHTML = '<div class="controls field_wrapper" style="margin:10px 0;"><input class="form-control" type="text" placeholder="Topic" name="topic[]" style="width:200px"/>&nbsp;<input class="form-control" type="text" placeholder="Subject" name="subject[]" style="width:200px"/>&nbsp;<input class="form-control" type="text" placeholder="Task" name="task[]" style="width:200px"/>&nbsp;<input class="form-control" type="date" placeholder="Date" name="dob[]" style="width:150px"/>&nbsp;<a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="fa fa-times"><i></a></div>'; //New input field html 
	    var x = 1; //Initial field counter is 1
	    $(addButton).click(function(){ //Once add button is clicked
	        if(x < maxField){ //Check maximum number of input fields
	            x++; //Increment field counter
	            $(wrapper).append(fieldHTML); // Add field html
	        }
	    });
	    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
	    	e.preventDefault();
	        $(this).parent('div').remove(); //Remove field html
	        x--; //Decrement field counter
	    });
	});


