<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Todo;


class StudentController extends Controller
{
  public function index()
  {
    $students = Student::all();
    $todos = Todo::all();
    return view('students.index',compact('students','todos'));
}

public function createPersonalInfo(Request $request)
{
    $student = $request->session()->get('student');

    return view('students.create-personal-info',compact('student'));
}


public function postPersonalInfo(Request $request)
{
    $validatedData = $request->validate([
        'first_name' => 'required',
        'last_name' => 'required',
        'contact_number' => 'required',
        'dob' => 'required',
        'email' => 'required|unique:students',
        'country' => 'required',
        'password'=>'min:6|required|same:confirm-password',
    ]);

    if(empty($request->session()->get('student'))){
        $student = new Student();
        $student->fill($validatedData);
        $request->session()->put('student', $student);
    }else{
        $student = $request->session()->get('student');
        $student->fill($validatedData);
        $request->session()->put('student', $student);
    }

    return redirect()->route('students.create-education-info');
}



public function createEducationDetail(Request $request)
{
    $student = $request->session()->get('student');

    return view('students.create-education-info',compact('student'));
}

    /**
     * Show the step One Form for creating a new student.
     *
     * @return \Illuminate\Http\Response
     */
    public function postcreateEducationDetail(Request $request)
    {
    	$validatedData = $request->validate([
            'qualification' => 'required',
            'school' => 'required',
            'date' => 'required',
            'grade' => 'required',
        ]);

        $student = $request->session()->get('student');
        $student->fill($validatedData);
        $request->session()->put('student', $student);

        return redirect()->route('students.create-submit');
    }



    public function createSubmit(Request $request)
    {
        $student = $request->session()->get('student');

        return view('students.create-submit',compact('student'));
    }

    
    public function postcreateSubmit(Request $request)
    {
        $student = $request->session()->get('student');
        $student->save();

        $request->session()->forget('student');

        return redirect()->route('students.index')->with('flash_message_success', 'Form has been added successfully');;
;
    }

    public function storeTodo(Request $request){


        if($request->isMethod('post')){
            $data = $request->all();


            foreach($data['topic'] as $key => $val){
                if(!empty($val)){

                    $todo = new Todo;
                    $todo->topic = $val;
                    $todo->subject = $data['subject'][$key];
                    $todo->task = $data['task'][$key];
                    $todo->dob = $data['dob'][$key];
                    $todo->save();
                }
            }
            return redirect()->route('students.index')->with('flash_message_success', 'Task has been added successfully');
        }

    }

}
