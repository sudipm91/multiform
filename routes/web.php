<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('students', [StudentController::class,'index'])->name('students.index');

Route::get('students/create-perosnal-info',[StudentController::class,'createPersonalInfo'] )->name('students.create-personal-info');
Route::post('students/create-personal-info', [StudentController::class,'postPersonalInfo'])->name('students.create-personal-info.post');

Route::get('students/create-education-info',[StudentController::class,'createEducationDetail'] )->name('students.create-education-info');
Route::post('students/create-education-info', [StudentController::class,'postcreateEducationDetail'])->name('students.create-education-info.post');

Route::get('students/create-submit',[StudentController::class,'createSubmit'] )->name('students.create-submit');
Route::post('students/create-submit', [StudentController::class,'postcreateSubmit'])->name('students.create-submit.post');

Route::post('add-todos',[StudentController::class,'storeTodo'])->name('add-todos');
