@extends('layout.frontLayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
           @include('forms.education_form')
        </div>
    </div>
</div>
@endsection