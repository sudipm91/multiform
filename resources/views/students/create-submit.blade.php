@extends('layout.frontLayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <form action="{{ route('students.create-submit.post') }}" method="post" >
                {{ csrf_field() }}
                <div class="card">
                    <div class="card-header">Step 3: Check Overview Student Information</div>

                    <div class="card-body">

                            <table class="table">
                                <tr>
                                    <td>First Name:</td>
                                    <td><strong>{{$student->first_name}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Last Name:</td>
                                    <td><strong>{{$student->last_name}}</strong></td>
                                </tr>
                                <tr>
                                    <td>DOB:</td>
                                    <td><strong>{{$student->dob}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Country:</td>
                                    <td><strong>{{$student->country}}</strong></td>
                                </tr>
                                 <tr>
                                    <td>Email:</td>
                                    <td><strong>{{$student->email}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Contact:</td>
                                    <td><strong>{{$student->contact_number}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Qualification:</td>
                                    <td><strong>{{$student->qualification}}</strong></td>
                                </tr>
                                <tr>
                                    <td>School:</td>
                                    <td><strong>{{$student->school}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Grade:</td>
                                    <td><strong>{{$student->grade}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Date:</td>
                                    <td><strong>{{$student->date}}</strong></td>
                                </tr>
                            </table>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <a href="{{ route('students.create-education-info')}}" class="btn btn-primary pull-right">Previous</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection