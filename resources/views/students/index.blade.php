@extends('layout.frontLayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Student Dashboard <i class="fa fa-tachometer"></i></div>

                <div class="card-body">

                    <a href="{{ route('students.create-personal-info') }}" class="btn btn-primary pull-right mb-3">Create student form <i class="fa fa-user"></i></a>

                    @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <table class="table table-responsive table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">DOB</th>
                                <th scope="col">Contact</th>
                                <th scope="col">Qualification</th>
                                <th scope="col">School</th>
                                <th scope="col">Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($students as $student)
                            <tr>
                                <th scope="row">{{$student->id}}</th>
                                <td>{{$student->first_name}}</td>
                                <td>{{$student->last_name}}</td>
                                <td>{{$student->email}}</td>
                                <td>{{$student->dob}}</td>
                                <td>{{$student->contact_number}}</td>
                                <td>{{$student->qualification}}</td>
                                <td>{{$student->school}}</td>
                                <td>{{$student->grade}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="mt-4 ">
             @include('forms.todo_form')
         </div>

     </div>
     <div class="col-md-12">
        <div class="card">
            <div class="card-header">Todo Multiple Button & Store  Data <i class="fa fa-list"></i></div>

            <div class="card-body">

                @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Topic</th>
                            <th>Subject</th>
                            <th>Task</th>
                            <th>Date</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($todos as $todo)
                        <tr>
                            <td>{{$todo->id}}</td>
                            <td>{{$todo->topic}}</td>
                            <td>{{$todo->subject}}</td>
                            <td>{{$todo->task}}</td>
                            <td>{{$todo->dob}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
</div>

@endsection