
@if (session()->has('flash_message_error'))
 <section class="text-center">
     <div class="row">
  <div class="col-lg-12">
          <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
      <h5> {{session()->get('flash_message_error')}}</h5>
      </div>
      </div>
</div>
</section>
@endif



