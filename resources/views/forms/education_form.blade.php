<form action="{{ route('students.create-education-info.post') }}" method="POST">
  {{ csrf_field() }}
  <div class="card">
    <div class="card-header">Step 2: Student Education Information</div>

    <div class="card-body">

      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="form-inline">
        <input type="text" placeholder="Qualification"   value="{{{ $student->qualification ?? '' }}}" class="form-control" id="qualification" name="qualification" style="width:200px" />&nbsp;
        <input type="text" placeholder="School"   value="{{{ $student->school ?? '' }}}" class="form-control" id="school" name="school" style="width:200px" />&nbsp;
        <input type="text" placeholder="Grade" value="{{{ $student->grade ?? '' }}}" class="form-control" id="grade" name="grade" style="width:200px" />&nbsp;
        <input type="date"  class="form-control" id="date" name="date" value="{{ $student->date ?? '' }}" style="width:150px" />&nbsp;
      </div>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-6 text-left">
          <a href="{{ route('students.create-personal-info') }}" class="btn btn-primary pull-right">Previous</a>
        </div>
        <div class="col-md-6 text-right">
          <button type="submit" class="btn btn-primary">Next</button>
        </div>
      </div>
    </div>
  </div>
</form>

