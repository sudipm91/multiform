<form action="{{ route('add-todos') }}" method="POST">
  {{ csrf_field() }}
  <div class="card">
    <div class="card-header">Add Multiple Button & Store  Data</div>

    <div class="card-body">

      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="form-inline">
        <label class="control-label"></label>
        <div class="field_wrapper">
          <input type="text" placeholder="Topic" class="form-control" id="topic" name="topic[]" style="width:200px" />
         <input type="text" placeholder="Subject" class="form-control" id="subject" name="subject[]" style="width:200px" />
          <input type="text" placeholder="Task" class="form-control" id="task" name="task[]" style="width:200px" />
         <input type="date"  class="form-control" id="dob" name="dob[]"  style="width:150px" />

         <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus"></i>Add</a>
       </div>
     </div>
   </div>
   <div class="card-footer">
    <div class="row">
      <div class="col-md-6 text-left">
       
      </div>
      <div class="col-md-6 text-right">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>
</form>

