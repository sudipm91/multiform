 <form action="{{ route('students.create-personal-info.post') }}" method="POST">
                {{ csrf_field() }}

                <div class="card">
                    <div class="card-header">Step 1: Student Personal Informations</div>

                    <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="first_name">First Name:</label>
                                <input type="text" value="{{ $student->first_name ?? '' }}" class="form-control" id="first_name"  name="first_name">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name:</label>
                                <input type="text"  value="{{ $student->last_name ?? '' }}" class="form-control" id="last_name" name="last_name"/>
                            </div>
                            <div class="form-group">
                                <label for="country">Country:</label>
                                <input type="text"  value="{{ $student->country ?? '' }}" class="form-control" id="country" name="country"/>
                            </div>
                            <div class="form-group">
                                <label for="date">Date Of Birth:</label>
                                <input type="date"  class="form-control" id="dob" name="dob" value="{{ $student->dob ?? '' }}" />
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email"  value="{{ $student->email ?? '' }}" class="form-control" id="email" name="email"/>
                            </div>
                            <div class="form-group">
                                <label for="contact_number">Contact Number:</label>
                                <input type="number"  value="{{ $student->contact_number ?? '' }}" class="form-control" id="contact_number" name="contact_number"/>
                            </div>
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password"  value="{{ $student->password ?? '' }}" class="form-control" id="password" name="password"/>
                            </div>

                             <div class="form-group">
                                <label for="password_confirmation">Confirm Password:</label>
                                <input type="password"  value="{{ $student->password ?? '' }}" class="form-control" id="confirm-password" name="confirm-password"/>
                            </div>

                        
                    </div>

                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">Next</button>
                    </div>
                </div>
            </form>